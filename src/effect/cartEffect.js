import { useStore } from 'vuex';
import { toRefs, computed } from 'vue';

export const useCommonCartEffect = (id) => {
  const store = useStore();
  const { cartList } = toRefs(store.state);
  const changeProductInfo = (productId, production, num) => {
    console.log(productId, production, num);
    store.commit('addProductToCart', { shopId: id, productId, production, num });
  };
  const productList = computed(() => {
    const productInfo = (cartList.value[id] && cartList.value[id].productList) || {};
    return productInfo;
  });
  const shopName = computed(() => {
    return cartList.value[id] && cartList.value[id].shopName;
  });
  const total = computed(() => {
    let count = 0;
    let price = 0;
    const productInfo = cartList.value[id] && cartList.value[id].productList;
    if (productInfo) {
      for (const i in productInfo) {
        count += productInfo[i].count;
        if (productInfo[i].checked) {
          price += productInfo[i].count * productInfo[i].price;
        }
      }
    }
    return { count, price: price.toFixed(2) };
  });
  return { changeProductInfo, cartList, productList, shopName, total };
};
