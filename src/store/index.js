import { createStore } from 'vuex';

export default createStore({
  state: {
    cartList: {
      // shopId: {
      // shopName: '',
      // productList: {
      //  productId: {}
      // }
      //   productId: {}
      // }
    }
  },
  getters: {},
  mutations: {
    // 添加/减少商品
    addProductToCart (state, payload) {
      const {
        shopId,
        productId,
        production,
        num
      } = payload;
      console.log(num);
      const cartList = state.cartList;
      const shopInfo = cartList[shopId] || { shopName: '', productList: {} };
      let productInfo = shopInfo.productList && shopInfo.productList[productId];

      if (!productInfo) {
        productInfo = production;
        productInfo.count = 0;
      }
      productInfo.count = productInfo.count + num;
      productInfo.checked = true;
      if (productInfo.count <= 0) { productInfo.count = 0; }
      // (productInfo.count <= 0) && (productInfo.count = 0)
      shopInfo.productList[productId] = productInfo;
      cartList[shopId] = shopInfo;
    },
    // 改变选中商品是否需要结算
    changeCheckedStatus (state, payload) {
      console.log(state, payload);
      const { productId, shopId } = payload;
      const cartList = state.cartList;
      const productList = cartList[shopId] && cartList[shopId].productList;
      productList[productId].checked = !productList[productId].checked;
    },
    // 清空购物车
    cleanCart (state, payload) {
      const { shopId } = payload;
      console.log(state, shopId);
      const cartList = state.cartList;
      cartList[shopId] = {};
    },
    // 清空或全选
    checkedAllOrClean (state, payload) {
      const { shopId, isCheckedAll } = payload;
      console.log(state, shopId, isCheckedAll);
      const cartList = state.cartList;
      const productList = cartList[shopId] && cartList[shopId].productList;
      for (const i in productList) {
        if (isCheckedAll) {
          productList[i].checked = false;
        } else {
          productList[i].checked = true;
        }
      }
    },
    // 改变名称
    changeShopName (state, payload) {
      console.log(state, payload);
      const { shopId, shopName } = payload;
      const cartList = state.cartList;
      if (!cartList[shopId]) { cartList[shopId].shopName = ''; cartList[shopId].productList = {}; }
      cartList[shopId].shopName = shopName;
    },
    // 清空商品列表
    clearProductList (state, payload) {
      console.log(state, payload);
      const { shopId } = payload;
      const cartList = state.cartList;
      cartList[shopId] = {};
    }
  },
  actions: {},
  modules: {},
});
