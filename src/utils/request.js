import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://www.fastmock.site/mock/ae8e9031947a302fed5f92425995aa19/jd',
  timeout: 10000,
});

export const get = (url, params = {}) => {
  return new Promise((resolve, reject) => {
    instance.get(url, { params }).then(response => {
      resolve(response.data);
    }).catch(err => {
      reject(err);
    });
  });
};

export const post = (url, params = {}) => {
  return new Promise((resolve, reject) => {
    instance.post(url, { ...params }, {
      headers: 'application/json'
    }).then(response => {
      resolve(response.data)
    }).catch(err => {
      console.log(err);
      reject(err)
    })
  })
}

// module.exports = {
//   get,
//   post
// }
