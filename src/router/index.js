import { createRouter, createWebHashHistory } from 'vue-router';
// import HomeView from "../views/HomeView.vue";

const routes = [
  {
    path: '/',
    name: 'Home',
    component: import(/* webpackChunkName: "Home" */ '../views/Home/Home.vue'),
    beforeEnter: (to, form, next) => {
      console.log(to, form);
      next();
      const isLogin = localStorage.getItem('isLogin');
      isLogin ? next({ name: 'Home' }) : next();
    }
  },
  {
    path: '/login',
    name: 'Login',
    component: import(/* webpackChunkName: "Login" */ '../views/Login/Login.vue'),
    beforeEnter: (to, form, next) => {
      console.log(to, form);
      const isLogin = localStorage.getItem('isLogin');
      const { name } = form;
      const redirect = name || 'Home';
      isLogin ? next({ name: redirect }) : next();
    }
  },
  {
    path: '/register',
    name: 'Register',
    component: import(/* webpackChunkName: "Login" */ '../views/Register/Register.vue'),
    beforeEnter: (to, form, next) => {
      console.log(to, form);
      const isLogin = localStorage.getItem('isLogin');
      const { name } = form;
      const redirect = name || 'Home';
      isLogin ? next({ name: redirect }) : next();
    }
  },
  {
    path: '/shop/:id',
    name: 'Shop',
    component: import(/* webpackChunkName: "Login" */ '../views/Shop/Shop.vue'),
    beforeEnter: (to, form, next) => {
      console.log(to, form);
      const isLogin = localStorage.getItem('isLogin');
      isLogin ? next() : next({ name: 'Login' });
    },
  },
  {
    path: '/orderConfimation/:id',
    name: 'OrderConfimation',
    component: import(/* webpackChunkName: "Login" */ '../views/OrderConfimation/OrderConfimation.vue'),
    beforeEnter: (to, form, next) => {
      console.log(to, form);
      const isLogin = localStorage.getItem('isLogin');
      isLogin ? next() : next({ name: 'Login' });
    }
  },
  {
    path: '/orderList',
    name: 'OrderList',
    component: import(/* webpackChunkName: "OrderList" */ '../views/OrderList/OrderList.vue'),
    beforeEnter: (to, form, next) => {
      console.log(to, form);
      const isLogin = localStorage.getItem('isLogin');
      isLogin ? next() : next({ name: 'Login' });
    }
  },
  {
    path: '/search',
    name: 'Search',
    component: import(/* webpackChunkName: "Search" */ '../views/Search/Search.vue'),
    beforeEnter: (to, form, next) => {
      console.log(to, form);
      const isLogin = localStorage.getItem('isLogin');
      isLogin ? next() : next({ name: 'Login' });
    }
  },
  {
    path: '/searchList/:keyWord',
    name: 'SearchList',
    component: import(/* webpackChunkName: "Search" */ '../views/SearchList/SearchList.vue'),
    beforeEnter: (to, form, next) => {
      console.log(to, form);
      const isLogin = localStorage.getItem('isLogin');
      isLogin ? next() : next({ name: 'Login' });
    }
  },
  {
    path: '/cartList',
    name: 'CartList',
    component: import(/* webpackChunkName: "Search" */ '../views/CartList/CartList.vue'),
    beforeEnter: (to, form, next) => {
      console.log(to, form);
      const isLogin = localStorage.getItem('isLogin');
      isLogin ? next() : next({ name: 'Login' });
    }
  },
  {
    path: '/address',
    name: 'Address',
    component: import(/* webpackChunkName: "Search" */ '../views/Address/Address.vue'),
    beforeEnter: (to, form, next) => {
      console.log(to, form);
      const isLogin = localStorage.getItem('isLogin');
      isLogin ? next() : next({ name: 'Login' });
    }
  },
  {
    path: '/addressEdit/:id',
    name: 'AddressEdit',
    component: import(/* webpackChunkName: "Search" */ '../views/AddressEdit/AddressEdit.vue'),
    beforeEnter: (to, form, next) => {
      console.log(to, form);
      const isLogin = localStorage.getItem('isLogin');
      isLogin ? next() : next({ name: 'Login' });
    }
  },
  {
    path: '/addressSelect',
    name: 'AddressSelect',
    component: import(/* webpackChunkName: "Search" */ '../views/AddressSelect/AddressSelect.vue'),
    beforeEnter: (to, form, next) => {
      console.log(to, form);
      const isLogin = localStorage.getItem('isLogin');
      isLogin ? next() : next({ name: 'Login' });
    }
  }
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

router.beforeEach((to, form, next) => {
  // console.log(form, to);
  const isLogin = localStorage.getItem('isLogin');
  const { name } = to;
  isLogin || name === 'Login' || name === 'Register' ? next() : next({ name: 'Login' });
});

export default router;
